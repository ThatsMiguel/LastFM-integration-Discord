/*
@author: poot#6431 -> Discord
*/

const DiscordRPC = require('discord-rpc');
const LastFmNode = require('lastfm').LastFmNode;
const LastFmName = ''; //change to your username
const snekfetch = require('snekfetch')
const rpc = new DiscordRPC.Client({ transport: 'ipc' });
const lastfm = new LastFmNode({
    api_key: '', //generate your LastFM api Key - Just Google it
    secret: '' //your secret key will be generated together
});

var trackStream = lastfm.stream(LastFmName);
trackStream.on('nowPlaying', (track) => {
    console.log('Now playing: ' + track.name + ' by ' + track.artist['#text']);
    setActivity(track, false);
});
trackStream.on('stoppedPlaying', (track) => {
    console.log('paused')
})
trackStream.on('scrobbled', (track) => {
    console.log('Scrobbled ' + track.name + ' by ' + track.artist['#text'])
    setActivity(track, true);
})
trackStream.on('error', (error) => {
    console.log('Error: ' + error.message);
});

const startTimestamp = new Date();

async function setActivity(track, scrobbled) {
    if (!rpc || !track) return;
    let smallImageKey = (scrobbled ? 'scrobble' : 'playing')
    rpc.setActivity({
        details: track.name,
        state: track.artist['#text'],
        largeImageKey: 'lastfm',
        largeImageText: 'https://www.last.fm/user/' + LastFmName,
        smallImageKey: smallImageKey,
        smallImageText: smallImageKey,
        instance: false
    });
}

rpc.on('ready', () => {
    trackStream.start();
});
const clientId = '472864200167784458';
const scopes = ['rpc', 'rpc.api'];
rpc.login({ clientId, scopes }).catch(err => console.log(err, /ERR/));